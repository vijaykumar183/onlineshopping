from rest_framework import serializers
from django.contrib.auth.models import User
from Backend.models import Products,Orders,OrderItems


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('title',
                  'Description',
                  'Image_link',
                  'Price',
                  'Created_At',
                  'Updated_At')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('UserId',
                  'Total',
                  'Created_At_Order',
                  'Updated_At_Order',
                  'Status','mode_of_payment')


class OrderItemsSerializers(serializers.ModelSerializer):
    class Meta:
        model = OrderItems
        fields = ('Order_Id','Product_Id','Quantity',
                  'price')