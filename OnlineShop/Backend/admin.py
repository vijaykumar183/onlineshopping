from django.contrib import admin

# Register your models here.
from Backend.models import Products,Orders,OrderItems


class Product_Admin(admin.ModelAdmin):
    show_fields = ['title','Description','Image_link','Price','Created_At','Updated_At']
admin.site.register(Products,Product_Admin)



class Orders_Admin(admin.ModelAdmin):
    list_fields =  ['UserId','Total','Created_At_Order','Updated_At_Order','Status','mode_of_payment']
admin.site.register(Orders,Orders_Admin)




class OrderItems_Admin(admin.ModelAdmin):
    display_fields = ['Order_Id','Product_Id','Quantity','price']
admin.site.register(OrderItems,OrderItems_Admin)
