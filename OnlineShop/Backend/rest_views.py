from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from Backend.models import Products
from Backend.serializers import ProductSerializer
from rest_framework import viewsets,generics
from rest_framework.permissions import IsAuthenticated


class ProductList(viewsets.ModelViewSet):
    def get_queryset(self):
        return Products.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    queryset =  Products.objects.all()
    serializer_class = ProductSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class ProductsDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Products.objects.filter(user=self.request.user)

    serializer_class = ProductSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
