from rest_framework import routers
from django.urls import path, include
from Backend.views import ProductList, OrderList, OrderItemsList

router = routers.DefaultRouter()
router.register('product', ProductList)
router.register('order', OrderList)
router.register('item', OrderItemsList)

urlpatterns = [
    path('', include(router.urls))
]
